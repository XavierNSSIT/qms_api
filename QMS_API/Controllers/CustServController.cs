﻿using QMS_API.Helpers;
using QMS_API.Models;
using QMS_API.Services;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace QMS_API.Controllers
{
//  [ApiExplorerSettings(IgnoreApi = true)]
    public class CustServController : ApiController
    {
        public IList<ZoneCounterType> GetMainScreen()
        { 
            GetNumberService service = new GetNumberService();
            IList<ZoneCounterType> model = service.TypeByZoneGet();
            
            return model;
        }

        public IList<string> GetTest()
        { 
            IList<string> strings = new List<string>();

            for (int i = 0; i < 10; i++)
            {
                strings.Add(i.ToString());
            }


            return strings;
        }

        public QueueNumbers GetQueueNo(string type, string driverName, string plateNo)
        {
            

            GetNumberService service = new GetNumberService();
            QueueNumbers number = service.QueueNoGet(type, driverName, plateNo);

            return number;
        }

        public IList<TransactionReport> GetTxnReport(string dateFrom, string dateTo)
        {
            ReportService service = new ReportService();
            IList<TransactionReport> report = service.TxnReportGet(DateTime.Parse(dateFrom), Convert.ToDateTime(dateTo).AddDays(1));

            return report;
        }

    }
}
