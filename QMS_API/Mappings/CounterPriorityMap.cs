﻿using QMS_API.Models;
using FluentNHibernate.Mapping;
using System;

namespace QMS_API.Mappings
{
    public class CounterPriorityMap : ClassMap<CounterPriority>
    {
        public CounterPriorityMap()
        {
            Table("COUNTERPRIORITY");

            this.DynamicUpdate();

            Id(p => p.Id);

            Map(p => p.MCounters_Id);
            Map(p => p.MTypes_Id);
            Map(p => p.Priority);
            Map(p => p.Status);
            Map(p => p.CreationId);
            Map(p => p.CreationTime);
            Map(p => p.LastModifiedId);
            Map(p => p.LastModifiedTime);
            Version(p => p.Version);
        }

    }

    public class CounterPriority_ReadMap : ClassMap<CounterPriority_Read>
    {
        public CounterPriority_ReadMap()
        {
            Table("COUNTERPRIORITY");

            this.ReadOnly();
           
            Id(p => p.Id);

            Map(p => p.MCounters_Id);
            Map(p => p.MTypes_Id);
            Map(p => p.Priority);
            Map(p => p.Status);
            Map(p => p.CreationId);
            Map(p => p.CreationTime);
            Map(p => p.LastModifiedId);
            Map(p => p.LastModifiedTime);
            Version(p => p.Version);

            References(p => p.Counter).Column("MCounters_Id").ReadOnly().LazyLoad();
            References(p => p.Type).Column("MTypes_Id").ReadOnly().LazyLoad();
        }

    }
}