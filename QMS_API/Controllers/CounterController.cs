﻿using QMS_API.Helpers;
using QMS_API.Models;
using QMS_API.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QMS_API.Controllers
{
    public class CounterController : ApiController
    {
        public IList<QueueNoStatusMain> GetQueueStatus(string counter)
        {
            CallNumberService service = new CallNumberService();
            IList<QueueNoStatusMain> queueStatus = service.QueueStatusMainGet(counter);

            return queueStatus;
        }

        public NextQueueNo GetNextQueueNo(string counter)
        {
            CallNumberService service = new CallNumberService();
            NextQueueNo nextQueueNo = service.QueueNoNext(counter);

            return nextQueueNo;
        }

        public ServiceResult GetResetNo()
        {
            CallNumberService service = new CallNumberService();
            ServiceResult result = service.ResetNo();

            return result;
        }
    }
}
