﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QMS_API.Models;

namespace QMS_API.Models
{
    public class ZoneCounterType
    {
        public MZonesShort Zone { get; set; }
        public IList<MTypesShort> Types { get; set; }
    }

    public class MZonesShort
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }

    public class MTypesShort
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }

    public class TypesByZone
    {
        public string MZones_Id { get; set; }
        public string MZones_Description { get; set; }
        public string MTypes_Id { get; set; }
        public string MTypes_Description { get; set; }
    }

}