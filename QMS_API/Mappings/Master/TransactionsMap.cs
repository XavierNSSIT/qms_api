﻿using QMS_API.Models;
using FluentNHibernate.Mapping;
using System;



namespace QMS_API.Mappings
{

    public class TransactionsMap : ClassMap<Transactions>
    {
        public TransactionsMap()
        {
            Table("TRANSACTIONS");

            this.DynamicUpdate();

            Id(p => p.Id);

            Map(p => p.Number);
            Map(p => p.MTypes_Id);
            Map(p => p.MCounters_Id);
            Map(p => p.ServedFlag);
            Map(p => p.CreationTime);
            Map(p => p.LastModifiedTime);
            Map(p => p.Driver_Name);
            Map(p => p.Driver_PlateNo);
            Map(p => p.QueueNo);
            Version(p => p.Version);
        }

    }



}

