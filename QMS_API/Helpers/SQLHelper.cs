﻿using System;
using System.Web;
using NHibernate;
using QMS_API.Models;
using System.Collections.Generic;
using NHibernate.Type;

namespace QMS_API.Helpers
{
    public static class SQLHelper
    {
      
        public static string SQLEncode(string parameter){
            if (parameter == null) {
                return "";
            }
            else
            {
                return parameter.Replace("'", "''");
            }
        }
        /*
        public static string JFilter(string jFilter, string jInField, string jSearchFr, string jSearchTo, string jSearchDateFr, string jSearchDateTo)
        {
            string SQL = "";
            switch (jFilter)
            {
                case "Range":
                    if (jSearchFr != null && jSearchFr != "")
                    {
                        SQL += " AND " + jInField + ">=:valueFr ";
                    }

                    if (jSearchTo != null && jSearchTo != "")
                    {
                        SQL += " AND " + jInField + "<=:valueTo ";
                    }


                    if (jSearchDateFr != null && jSearchDateFr != "")
                    {
                        SQL += " AND " + jInField + ">=:valueDateFr ";
                    }

                    if (jSearchDateTo != null && jSearchDateTo != "")
                    {
                        SQL += " AND " + jInField + "<:valueDateTo ";
                    }
                    break;
                case "EqualTo":
                    if (jSearchFr != null && jSearchFr != "")
                    {
                        SQL += " AND " + jInField + "=:valueFr ";
                    }

                    if (jSearchDateFr != null && jSearchDateFr != "")
                    {
                        SQL += " AND " + jInField + ">=:valueDateFr AND "  + jInField + "<:valueDateTo ";
                    }
                    break;
                case "Contain":
                case "StartWith":
                case "EndWith":
                    if (jSearchFr != null && jSearchFr != "")
                    {
                        SQL += " AND " + jInField + " like :valueFr ";
                    }
                    break;

            }

            return SQL;
        }
         * */


        public static string JFilter(string jFilter, string jInField, string jSearchFr, string jSearchTo, string jSearchDateFr, string jSearchDateTo)
        {
            string SQL = "";
            switch (jFilter)
            {
                case "Range":
                 if (jSearchFr != null && jSearchFr != "")
                    {
                        SQL += " AND " + jInField + ">=:valueFr ";
                    }

                    if (jSearchTo != null && jSearchTo != "")
                    {
                        SQL += " AND " + jInField + "<=:valueTo ";
                    }



                    if (jSearchDateFr != null && jSearchDateFr != "")
                    {
                        DateTime dateFrom = Convert.ToDateTime(jSearchDateFr.Trim());


                        SQL += " AND " + jInField + ">='" + SQLEncode(dateFrom.ToSQLFormat()) + "' ";
                    }

                    if (jSearchDateTo != null && jSearchDateTo != "")
                    {
                        DateTime dateTo = Convert.ToDateTime(jSearchDateTo.Trim()).AddDays(1);
     

                        SQL += " AND " + jInField + "<'" + SQLEncode(dateTo.ToSQLFormat()) + "' ";
                    }
                    break;
                case "EqualTo":
                    if (jSearchFr != null && jSearchFr != "")
                    {
                        SQL += " AND " + jInField + " = :valueFr ";
                    }

                    if (jSearchDateFr != null && jSearchDateFr != "")
                    {

                        DateTime dateFrom = Convert.ToDateTime(jSearchDateFr.Trim());      
                        DateTime dateTo = Convert.ToDateTime(jSearchDateFr.Trim()).AddDays(1);
                  
                        SQL += " AND " + jInField + ">='" + SQLEncode(dateFrom.ToSQLFormat()) + "' AND " + jInField + "<'" + SQLEncode(dateTo.ToSQLFormat()) + "' ";
                    }
                    break;
                case "Contain":
                case "StartWith":
                case "EndWith":
                    if (jSearchFr != null && jSearchFr != "")
                    {
                        SQL += " AND " + jInField + " like :valueFr escape '\\' ";
                    }                    break;

            }

            return SQL;
        }
        public static string JStatus(string jStatus, string prefix) {
            string SQL = "";
            if (jStatus != null && jStatus.Trim() != "") {
                SQL += " AND " + prefix + "Status='" + jStatus + "' ";
            }
          

            return SQL;
        }

        public static string JCustomStatus(string jStatus, string prefix, string columeName)
        {
            string SQL = "";
            if (jStatus != null && jStatus.Trim() != "")
            {
                SQL += " AND " + prefix + columeName + "='" + jStatus + "' ";
            }


            return SQL;
        }

        public static void JSetParameter(IQuery query, string jFilter, string jSearchFr, string jSearchTo, string jSearchDateFr, string jSearchDateTo)
        {
          
            switch (jFilter)
            {
                case "Range":
                    if (jSearchFr != null && jSearchFr != "")
                    {
                        //query.SetParameter("valueFr", jSearchFr.Trim(), TypeFactory.GetAnsiStringType(500));
                        query.SetParameter("valueFr", jSearchFr.Trim());
                    }

                    if (jSearchTo != null && jSearchTo != "")
                    {
                        //query.SetParameter("valueTo", jSearchTo.Trim(), TypeFactory.GetAnsiStringType(500));
                        query.SetParameter("valueTo", jSearchTo.Trim());
                    }

                    /*
                    if (jSearchDateFr != null && jSearchDateFr != "")
                    {
                        DateTime dateFrom=Convert.ToDateTime(jSearchDateFr.Trim());
                        dateFrom=dateFrom.ToUTCTime();

                        query.SetParameter("valueDateFr", dateFrom);
                    }

                    if (jSearchDateTo != null && jSearchDateTo != "")
                    {

                        DateTime dateTo = Convert.ToDateTime(jSearchDateTo.Trim());
                        dateTo = dateTo.AddDays(1).ToUTCTime();

                        query.SetParameter("valueDateTo", dateTo);
                    }
                     * */

                    break;
                case "EqualTo":
                    if (jSearchFr != null && jSearchFr != "")
                    {
                        query.SetParameter("valueFr", jSearchFr.Trim());
                    }

                    /*
                    if (jSearchDateFr != null && jSearchDateFr != "")
                    {
                        DateTime dateFrom = Convert.ToDateTime(jSearchDateFr.Trim());
                        dateFrom = dateFrom.ToUTCTime();

                        query.SetParameter("valueDateFr", dateFrom);

                        query.SetParameter("valueDateTo", dateFrom.AddDays(1));
                    }
                     * 
                     * */

                    break;
                case "Contain":
                    if (jSearchFr != null && jSearchFr != "")
                    {
                        query.SetParameter("valueFr", "%" + LikeEscape(jSearchFr.Trim()) + "%");
                    }
                    break;
                case "StartWith":
                    if (jSearchFr != null && jSearchFr != "")
                    {
                        query.SetParameter("valueFr", LikeEscape(jSearchFr.Trim()) + "%");
                    }
                    break;
                case "EndWith":
                    if (jSearchFr != null && jSearchFr != "")
                    {
                        query.SetParameter("valueFr", "%" + LikeEscape(jSearchFr.Trim()));
                    }
                    break;
            }


           
        }


        private static string LikeEscape(string likeValue)
        {
            return likeValue.Replace("\\", "\\\\").Replace("_", "\\_").Replace("%", "\\%").Replace("[", "\\[");
        }

        public static int CheckPageValue(int page, int totalCount, int recordPerPage)
        {
            if (page < 1) {
                return 1;
            }
            
            double totalPage = Math.Ceiling((double)totalCount / (double)recordPerPage);
            if (page > totalPage) { 
                page=(int)totalPage;
            }
            return page;
        }



        public static int FirstFreeNumber(string FirstFreeSettings_Id, ISession session)
        {
            string prefix = "";
            int length = 0;
            int runningNumber = 0;
            int firstFreeNumber = 0;
            int maxNum = 0;

            MFirstFreeSettings firstFreeSetting = session.QueryOver<MFirstFreeSettings>().Where(x => x.Id == FirstFreeSettings_Id).SingleOrDefault();

            if (firstFreeSetting == null)
            {
                return firstFreeNumber;
            }

            length = firstFreeSetting.Length;
            prefix = firstFreeSetting.Prefix;
            maxNum = Int32.Parse(new string('9', firstFreeSetting.Length)) + 1;

            firstFreeSetting = session.Get<MFirstFreeSettings>(firstFreeSetting.Id, LockMode.Upgrade);


            MFirstFreeNumbers firstFreeNo = session.QueryOver<MFirstFreeNumbers>().Where(x => x.FirstFreeSettings_Id == FirstFreeSettings_Id).And(x => x.LatestFlag == ACTIVE_STATUS.Active)
                                                .SingleOrDefault();

            if (firstFreeNo == null)
            {
                MFirstFreeNumbers firstFreeNo_New = new MFirstFreeNumbers()
                {
                    FirstFreeSettings_Id = FirstFreeSettings_Id,
                    RunningNo = 2,
                    LastModifiedId = SYSTEM.AccountId,
                    CreationId = SYSTEM.AccountId,
                    MTypes_Id = FirstFreeSettings_Id,
                    LatestFlag = ACTIVE_STATUS.Active
                };

                session.Save(firstFreeNo_New);
                runningNumber = 1;
            }
            else
            {                
                runningNumber = firstFreeNo.RunningNo;
                if (runningNumber == maxNum)
                {
                    firstFreeNo.RunningNo = 2;
                    runningNumber = 1;
                }
                else
                {
                    firstFreeNo.RunningNo = runningNumber + 1;
                }
                
                session.Update(firstFreeNo);
            }

            //string runningNumberFormat = "";
            switch (FirstFreeSettings_Id)
            {
                default:
                    //runningNumberFormat = new string('0', length) + runningNumber.ToString();
                    firstFreeNumber = runningNumber;
                    break;
            }
            return firstFreeNumber;
        }

      
    }
}