﻿using System;
using System.IO;

namespace QMS_API.Helpers
{
    public class LogHelper 
    {
        //private string current_path = System.Configuration.ConfigurationManager.AppSettings["BankLogPath"];
        private string log_path, log_filename_path;

        DateTime d = DateTimeHelper.GetNow();
        string currDT ;

        public LogHelper(string current_path, string log_filename)
        {
            currDT = String.Format("{0:yyyyMMdd}", d);
            
            log_path = current_path + "\\Log";
            log_filename = log_filename + "_" + currDT + ".txt";

            log_filename_path = log_path + "\\" + log_filename;

            try
            {
                if (!Directory.Exists(log_path))
                {

                     Directory.CreateDirectory(log_path);
                
                }
                if (!File.Exists(log_filename_path))
                {
                    StreamWriter sw =File.CreateText(log_filename_path);
                    sw.Close();
                }
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
        }

        public void Write(string text)
        {

            try
            {
                StreamWriter sw = File.AppendText(log_filename_path);                
                sw.WriteLine("");
                sw.WriteLine(String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTimeHelper.GetNow()));
                sw.WriteLine(text);
                sw.Close();
                
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
        }

        public static void ErrorLog(Exception e)
        {
            log4net.LogManager.GetLogger("LogError").Error(string.Empty, e);

        }

        public static void ErrorLog(string errorMesg)
        {
            log4net.LogManager.GetLogger("LogError").Error(errorMesg);

        }


        public static void WebServiceErrorLog(string WS_Name, string error)
        {
            log4net.LogManager.GetLogger("LogError").Error("WebService " + WS_Name + " : " + error);

        }

        public static void WSLog(string message)
        {
            log4net.LogManager.GetLogger("LogError").Error(" " + message);
        }
    }
}
