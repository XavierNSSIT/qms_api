﻿using QMS_API.Models;
using FluentNHibernate.Mapping;
using System;



namespace QMS_API.Mappings
{

    public class CurrentServingNumbersMap : ClassMap<CurrentServingNumbers>
    {
        public CurrentServingNumbersMap()
        {
            Table("CURRENTSERVINGNUMBERS");

            this.DynamicUpdate();

            Id(p => p.Id);

            Map(p => p.MTypes_Id);
            Map(p => p.Number);
            Map(p => p.CreationTime);
            Version(p => p.Version);
        }

    }



}

