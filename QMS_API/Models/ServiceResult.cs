﻿using System;

namespace QMS_API.Models
{
    public class ServiceResult
    {
        public ServiceStatus Status { get; set; }

        public string ErrorMessage { get; set; }


        public string ReferenceNo { get; set; }
    }
}