﻿using System;

namespace QMS_API.Models
{
    public class MFirstFreeSettings
    {
        public virtual string Id { get; set; }
        public virtual int Length { get; set; }
        public virtual string Prefix { get; set; }
        public virtual int Version { get; set; }
    }

}
