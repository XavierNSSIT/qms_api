﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QMS_API.Models;
using NHibernate;
using FluentNHibernate;
using QMS_API.Helpers;
using NHibernate.Transform;

namespace QMS_API.Services
{
    public class GetNumberService
    {
        public IList<ZoneCounterType> TypeByZoneGet()
        {
            IList<ZoneCounterType> model = new List<ZoneCounterType>();
            ZoneCounterType modelSingle = null;
            MZonesShort zoneSingle = null;
            IList<MZones> zones = null;

            using (ISession session = NHibernateHelper.OpenSession())
            {
                zones = session.QueryOver<MZones>().Where(x => x.Status == ACTIVE_STATUS.Active).List();
            }

            for (int i = 0; i < zones.Count; i++)
            {
                zoneSingle = new MZonesShort(){ 
                    Id = zones[i].Id,
                    Description = zones[i].Description
                };

                modelSingle = new ZoneCounterType() {
                    Zone = zoneSingle,
                    Types = TypesGet(zoneSingle.Id)
                };
                

                model.Add(modelSingle);
            }

            return model;
        }

        public IList<MTypesShort> TypesGet(string Id)
        {
            CounterPriority_Read cPrio = null;
            MCounters_Read counter = null;
            MTypes type = null;
            MZones zone = null;

            IList<MTypesShort> typeShorts = new List<MTypesShort>();
            MTypesShort typeShort = null;

            using (ISession session = NHibernateHelper.OpenSession())
            {
                typeShorts = session.QueryOver(() => cPrio)
                            .JoinAlias(p => p.Counter, () => counter)
                            .JoinAlias(p => p.Type, () => type)
                            .JoinAlias(() => counter.Zone, () => zone)
                            .SelectList(list => list
                                .SelectGroup(() => type.Id).WithAlias(() => typeShort.Id)
                                .SelectGroup(() => type.Description).WithAlias(() => typeShort.Description))
                            .Where(() => cPrio.Status == ACTIVE_STATUS.Active)
                            .And(() => counter.Status == ACTIVE_STATUS.Active)
                            .And(() => zone.Status == ACTIVE_STATUS.Active)
                            .And(() => type.Status == ACTIVE_STATUS.Active)
                            .And(() => zone.Id == Id)
                            .TransformUsing(Transformers.AliasToBean<MTypesShort>())
                            .List<MTypesShort>();
            }

            return typeShorts;
        }


        public QueueNumbers QueueNoGet(string type, string driverName, string plateNo)
        {
            QueueNumbers number = new QueueNumbers();

            ISession session = NHibernateHelper.OpenSession();

            using (ITransaction transaction = session.BeginTransaction())
            {
                try
                {

                    int qNo = SQLHelper.FirstFreeNumber(type, session);
                    
                    CurrentServingNumbers currNo = session.QueryOver<CurrentServingNumbers>().Where(x => x.MTypes_Id == type).SingleOrDefault();
                    MFirstFreeSettings settings = session.QueryOver<MFirstFreeSettings>().Where(x => x.Id == type).SingleOrDefault();


                    string qNoStr = new string('0', settings.Length) + qNo.ToString();
                    number.QueueNo = settings.Prefix + qNoStr.Substring(qNoStr.Length - settings.Length, settings.Length);

                    string runningNumberFormat = "";
                    int currNum = 1;

                    //if (currNo == null)
                    //{
                    //    runningNumberFormat = new string('0', settings.Length) + "0";
                    //}
                    //else
                    //{
                    //    runningNumberFormat = new string('0', settings.Length) + currNo.Number.ToString();
                    //    currNum = currNo.Number + 1;
                    //}
                    if (currNo != null)
                    {
                        runningNumberFormat = new string('0', settings.Length) + currNo.Number.ToString();
                        currNum = currNo.Number + 1;
                        number.CurrentServingNo = settings.Prefix + runningNumberFormat.Substring(runningNumberFormat.Length - settings.Length, settings.Length);
                    }
                    else
                    {
                        number.CurrentServingNo = "-";
                    }

                    number.Type = type;
                    number.DriverName = driverName;
                    number.PlateNo = plateNo;

                    Transactions txn = new Transactions()
                    {
                        Number = qNo,
                        MCounters_Id = "",
                        MTypes_Id = type,
                        ServedFlag = ACTIVE_STATUS.Inactive,
                        Driver_Name = driverName,
                        Driver_PlateNo = plateNo,
                        QueueNo = number.QueueNo
                    };

                    session.Save(txn);

                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                }
            }
        
            session.Dispose();

            return number;
        }
    }
}