﻿using System;

namespace QMS_API.Models
{
    public static class ACTIVE_STATUS
    {
        public const string Active = "1";
        public const string Inactive = "0";
        public const string Expired = "2";
    }

    public enum ServiceStatus
    {
        None = -1,
        Success = 0,
        DataNotFound = 1,
        DuplicateKey = 2,
        OtherErrors = 3,
        ReferenceConstraint = 4,
        SystemError = 5,
        FailLogin = 6,
        SignatureNotMatch = 7,
        PaymentUnsuccess = 8,
        PaymentFail = 9,
        ConfirmSeatError = 10,
        PaymentPendingConfirmation = 11,
    }

    public static class SYSTEM
    {
        public static readonly string AccountId = "system";
    }

    public static class FREEFIRSTNUMBERID
    {
        public const string PaymentBatchId = "PaymentBatchId";
        public const string PaymentDetailBatchId = "PaymentDetailBatchId";
        public const string CompanyBatchId = "CompanyBatchId";
      
    }
}