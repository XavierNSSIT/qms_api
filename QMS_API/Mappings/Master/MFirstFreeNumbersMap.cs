﻿using QMS_API.Models;
using FluentNHibernate.Mapping;
using System;


namespace QMS_API.Mappings
{

    public class MFirstFreeNumbersMap : ClassMap<MFirstFreeNumbers>
    {
        public MFirstFreeNumbersMap()
        {
            Table("MFIRSTFREENUMBERS");

            this.DynamicUpdate();

            Id(p => p.Id).GeneratedBy.GuidComb();

            Map(p => p.FirstFreeSettings_Id);
            Map(p => p.RunningNo);
            Map(P => P.LatestFlag);

            Map(p => p.MTypes_Id);
            Map(p => p.LastModifiedTime);
            Map(p => p.LastModifiedId);
            Map(p => p.CreationTime);
            Map(p => p.CreationId);
            Version(p => p.Version);

           
        }

    }


  
}

