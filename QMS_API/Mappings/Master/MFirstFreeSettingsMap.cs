﻿using QMS_API.Models;
using FluentNHibernate.Mapping;
using System;



namespace QMS_API.Mappings
{

    public class MFirstFreeSettingsMap : ClassMap<MFirstFreeSettings>
    {
        public MFirstFreeSettingsMap()
        {
            Table("MFIRSTFREESETTINGS");

            this.DynamicUpdate();

            Id(p => p.Id);

            Map(p => p.Length);
            Map(p => p.Prefix);
            Version(p => p.Version);
        }

    }


  
}

