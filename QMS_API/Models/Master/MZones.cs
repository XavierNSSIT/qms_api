﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QMS_API.Models
{
    public class MZones
    {
        DateTime _CreationTime;
        DateTime _LastModifiedTime;
        public virtual string Id { get; set; }
        public virtual string Description { get; set; }
        public virtual string Status { get; set; }
        public virtual string CreationId { get; set; }
        public virtual string LastModifiedId { get; set; }
        public virtual DateTime LastModifiedTime
        {
            get { return _LastModifiedTime; }
            set { _LastModifiedTime = value; }
        }
        public virtual DateTime CreationTime
        {
            get { return _CreationTime; }
            set { _CreationTime = value; }
        }
        public virtual int Version { get; set; }
    }
}