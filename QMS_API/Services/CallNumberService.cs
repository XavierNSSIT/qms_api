﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QMS_API.Models;
using NHibernate;
using FluentNHibernate;
using QMS_API.Helpers;
using NHibernate.Transform;
using System.Net.Http;

namespace QMS_API.Services
{
    public class CallNumberService
    {
        public IList<QueueNoStatusMain> QueueStatusMainGet(string counterName)
        {
            string[] counterNames = counterName.Split(',');
            IList<QueueNoStatusMain> queueStatusesMain = new List<QueueNoStatusMain>();
            QueueNoStatusMain queueStatusMain = null;
            IList<QueueNoStatus> queueStatuses = new List<QueueNoStatus>();
            QueueNoStatus queueStatus = null;
            int currNo = 0;
            int numCalled = 0;
            int length = 0;
            int maxNo = 0;
            int numDiff = 0;
            int queueCount = 0;
            Transactions txnDetail = new Transactions();

            try
            {
                using (ISession session = NHibernateHelper.OpenSession())
                {
                    for (int j = 0; j < counterNames.Length; j++)
                    {
                        MCounters counter = session.QueryOver<MCounters>().Where(x => x.Id == counterNames[j]).SingleOrDefault();

                        if (counter != null && counter.Status != "0")
                        {
                            IList<string> cPrios = session.QueryOver<CounterPriority>().Select(x => x.MTypes_Id).Where(x => x.MCounters_Id == counterNames[j]).List<string>();

                            queueStatusMain = new QueueNoStatusMain()
                            {
                                Counter = counter.Id,
                                QueueNoStatus = new List<QueueNoStatus>()
                            };

                            for (int i = 0; i < cPrios.Count; i++)
                            {
                                string aaaaaa = cPrios[i];
                                currNo = session.QueryOver<CurrentServingNumbers>().Select(x => x.Number).Where(x => x.MTypes_Id == cPrios[i]).SingleOrDefault<int>();
                                //txnDetail = session.QueryOver<Transactions>().Where(x => x.MTypes_Id == cPrios[i]).And(x => x.ServedFlag == ACTIVE_STATUS.Inactive).And(x => x.Number ==currNo).SingleOrDefault();
                                length = session.QueryOver<MFirstFreeSettings>().Select(x => x.Length).Where(x => x.Id == cPrios[i]).SingleOrDefault<int>();
                                numCalled = session.QueryOver<MFirstFreeNumbers>().Select(x => x.RunningNo).Where(x => x.MTypes_Id == cPrios[i]).And(x => x.LatestFlag == ACTIVE_STATUS.Active).SingleOrDefault<int>() - 1;
                                numDiff = numCalled - currNo;
                                maxNo = Int32.Parse(new string('9', length));

                                if (numDiff < 0)
                                {
                                    if (numCalled == -1 && currNo == 0)
                                        queueCount = 0;
                                    else
                                        queueCount = numDiff + maxNo;
                                }
                                else
                                {
                                    queueCount = numDiff;
                                }

                                queueStatus = new QueueNoStatus()
                                {
                                    QueueType = cPrios[i],
                                    TotalQueue = queueCount,                                    
                                };

                                //queueStatuses.Add(queueStatus);
                                queueStatusMain.QueueNoStatus.Add(queueStatus);
                            }

                            queueStatusesMain.Add(queueStatusMain);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                LogHelper.ErrorLog(e);
            }

            return queueStatusesMain;
        }

        public NextQueueNo QueueNoNext(string counterName)
        {
            NextQueueNo nextNo = null;
            Transactions txn = null;
            CurrentServingNumbers currNo = null;
            MFirstFreeSettings settings = null;
            int numbers = 0;
            int numDiff = 0;
            bool PIDSOK = false;

            using (ISession session = NHibernateHelper.OpenSession())
            { 
                using(ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        MCounters counter = session.QueryOver<MCounters>().Where(x => x.Id == counterName).SingleOrDefault();

                        if (counter != null)
                        {
                            IList<string> cPrios = session.QueryOver<CounterPriority>().Select(x => x.MTypes_Id)
                                                    .Where(x => x.MCounters_Id == counterName)
                                                    .OrderBy(x => x.Priority).Asc.List<string>();

                            foreach (var item in cPrios)
                            {
                                MTypes typeLock = session.Get<MTypes>(item, LockMode.Upgrade);
                                //MTypes typeLock = session.QueryOver<MTypes>().Where(x => x.Id == item).SingleOrDefault();

                                settings = session.QueryOver<MFirstFreeSettings>().Where(x => x.Id == item).SingleOrDefault();
                                currNo = session.QueryOver<CurrentServingNumbers>().Where(x => x.MTypes_Id == item).SingleOrDefault();
                                numbers = session.QueryOver<MFirstFreeNumbers>().Select(x => x.RunningNo).Where(x => x.MTypes_Id == item).And (x => x.LatestFlag == ACTIVE_STATUS.Active).SingleOrDefault<int>() - 1;

                                if (currNo == null)
                                {
                                    if (numbers >= -1)
                                    {
                                        txn = session.QueryOver<Transactions>().Where(x => x.Number == 1)
                                        .And(x => x.ServedFlag == ACTIVE_STATUS.Inactive).And(x => x.MTypes_Id == item).SingleOrDefault();

                                        if (txn != null)
                                        {
                                            txn = session.Get<Transactions>(txn.Id, LockMode.Upgrade);
                                            //txn = session.Get<Transactions>(txn.Id);
                                            txn.LastModifiedTime = DateTimeHelper.GetNow();
                                            txn.MCounters_Id = counterName;
                                            txn.ServedFlag = ACTIVE_STATUS.Active;

                                            session.Update(txn);

                                            currNo = new CurrentServingNumbers()
                                            {
                                                MTypes_Id = item,
                                                Number = 1,
                                                CreationTime = DateTimeHelper.GetNow()
                                            };

                                            session.Save(currNo);

                                            string runningNumberFormat = new string('0', settings.Length) + currNo.Number.ToString();
                                            string runningNum = settings.Prefix + runningNumberFormat.Substring(runningNumberFormat.Length - settings.Length, settings.Length);

                                            nextNo = new NextQueueNo()
                                            {
                                                QueueNo = runningNum,
                                                QueueType = typeLock.Description,
                                                DriverName = txn.Driver_Name,
                                                PlateNo = txn.Driver_PlateNo
                                            };

                                            QueueNoForQDS passModel = new QueueNoForQDS()
                                            {
                                                queueNo = runningNum,
                                                counterNo = counter.Description,
                                            };

                                            int count = 0;
                                            string result = "OK";
                                            //while (!PIDSOK || count < 4)
                                            //{
                                            //    result = UpdatePIDS(passModel);
                                            //    if (result == "OK")
                                            //    {
                                            //        PIDSOK = true;
                                            //    }
                                            //    count++;
                                            //}

                                            if (result == "OK")
                                            {
                                                transaction.Commit();
                                            }
                                            else
                                            {
                                                transaction.Rollback();
                                            }
                                            
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    numDiff = numbers - currNo.Number;

                                    if (numDiff != 0)
                                    {
                                        int num = 0;

                                        if(numDiff < 0 && currNo.Number == Int32.Parse(new string('9', settings.Length)))
                                        {
                                            num = 1;
                                        }
                                        else
                                        {
                                            num = currNo.Number + 1;
                                        }
                                        txn = session.QueryOver<Transactions>().Where(x => x.Number == num)
                                        .And(x => x.ServedFlag == ACTIVE_STATUS.Inactive).And(x => x.MTypes_Id == item).SingleOrDefault();

                                        if (txn != null)
                                        {
                                            txn = session.Get<Transactions>(txn.Id, LockMode.Upgrade);
                                            //txn = session.Get<Transactions>(txn.Id);
                                            txn.LastModifiedTime = DateTimeHelper.GetNow();
                                            txn.MCounters_Id = counterName;
                                            txn.ServedFlag = ACTIVE_STATUS.Active;

                                            session.Update(txn);


                                            currNo.Number = num;
                                            currNo.CreationTime = DateTimeHelper.GetNow();
                                            session.Update(currNo);

                                            string runningNumberFormat = new string('0', settings.Length) + num.ToString();
                                            string runningNum = settings.Prefix + runningNumberFormat.Substring(runningNumberFormat.Length - settings.Length, settings.Length);

                                            nextNo = new NextQueueNo()
                                            {
                                                QueueNo = runningNum,
                                                QueueType = typeLock.Description,
                                                DriverName = txn.Driver_Name,
                                                PlateNo = txn.Driver_PlateNo
                                            };

                                            QueueNoForQDS passModel = new QueueNoForQDS()
                                            {
                                                queueNo = runningNum,
                                                counterNo = counter.Description
                                            };

                                            int count = 0;
                                            string result = "OK";
                                            //while (!PIDSOK || count < 4)
                                            //{
                                            //    result = UpdatePIDS(passModel);
                                            //    if (result == "OK")
                                            //    {
                                            //        PIDSOK = true;
                                            //    }
                                            //    count++;
                                            //}

                                            if (result == "OK")
                                            {
                                                transaction.Commit();
                                            }
                                            else
                                            {
                                                transaction.Rollback();
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        LogHelper.ErrorLog(e);
                        transaction.Rollback();
                    }
                }
            }

            return nextNo;
        }

        public string UpdatePIDS(QueueNoForQDS model)
        {
            string result2 = "";
            try
            {
                //var client = new HttpClient();
                //client.BaseAddress = new Uri("http://192.168.1.16/");
                //HttpResponseMessage response = await client.GetAsync("QDS/Home/UpdateNewQueueNo?queueNo=" + QueueNo + "&counterNo=" + CounterNo);
                //string result2 = await response.Content.ReadAsStringAsync();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost/QDS/");

                    //HTTP POST
                    var postTask = client.PostAsJsonAsync("Home/UpdateNewQueueNo/", model);
                    postTask.Wait();

                    var result = postTask.Result;
                    result2 = result.StatusCode.ToString();
                }
            }
            catch (Exception e)
            {
                LogHelper.ErrorLog(e);
            }

            return result2;
        }

        public ServiceResult ResetNo()
        {
            ServiceResult result = new ServiceResult();

            IList<Transactions> activeTxn = new List<Transactions>();
            IList<MFirstFreeNumbers> firstFreeNumbers = new List<MFirstFreeNumbers>();
            IList<CurrentServingNumbers> currNumbers = new List<CurrentServingNumbers>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        activeTxn = session.QueryOver<Transactions>().Where(x => x.ServedFlag == ACTIVE_STATUS.Inactive).Lock().Upgrade.List().ToList();

                        if (activeTxn.Count > 0)
                        {
                            foreach (var txn in activeTxn)
                            {
                                txn.ServedFlag = ACTIVE_STATUS.Expired;
                                session.Update(txn);
                            }
                            
                        }

                        firstFreeNumbers = session.QueryOver<MFirstFreeNumbers>().Where(x => x.LatestFlag == ACTIVE_STATUS.Active).Lock().Upgrade.List().ToList();

                        if (firstFreeNumbers.Count > 0)
                        {
                            foreach (var number in firstFreeNumbers)
                            {
                                number.LatestFlag = ACTIVE_STATUS.Inactive;
                                session.Update(number);
                            }
                            
                        }

                        currNumbers  = session.QueryOver<CurrentServingNumbers>().Lock().Upgrade.List().ToList();

                        if (currNumbers.Count > 0)
                        {
                            foreach ( var currNumber in currNumbers)
                            {
                                currNumber.Number = 0;
                                session.Update(currNumber);
                            }
                        }


                        transaction.Commit();
                        result.Status = ServiceStatus.Success;
                    }
                    catch (Exception e)
                    {
                        LogHelper.ErrorLog("CallNumberServices.ResetNo: " + e);
                        transaction.Rollback();
                        result.Status = ServiceStatus.OtherErrors;
                        result.ErrorMessage = "Failed to Reset";
                    }
                }
            }

            return result;
        }
    }
}