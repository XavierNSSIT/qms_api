﻿using QMS_API.Models;
using FluentNHibernate.Mapping;
using System;



namespace QMS_API.Mappings
{

    public class MCountersMap : ClassMap<MCounters>
    {
        public MCountersMap()
        {
            Table("MCOUNTERS");

            this.DynamicUpdate();

            Id(p => p.Id);

            Map(p => p.Description);
            Map(p => p.MZones_Id);
            Map(p => p.Status);
            Map(p => p.CreationId);
            Map(p => p.CreationTime);
            Map(p => p.LastModifiedId);
            Map(p => p.LastModifiedTime);
            Version(p => p.Version);
        }

    }


    public class MCounters_ReadMap : ClassMap<MCounters_Read>
    {
        public MCounters_ReadMap()
        {
            Table("MCOUNTERS");

            this.ReadOnly();
         
            Id(p => p.Id);

            Map(p => p.Description);
            Map(p => p.MZones_Id);
            Map(p => p.Status);
            Map(p => p.CreationId);
            Map(p => p.CreationTime);
            Map(p => p.LastModifiedId);
            Map(p => p.LastModifiedTime);
            Version(p => p.Version);

            References(p=>p.Zone).Column("MZones_Id").ReadOnly().LazyLoad();
            
        }

    }

  
}

