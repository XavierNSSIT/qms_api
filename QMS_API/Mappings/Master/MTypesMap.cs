﻿using QMS_API.Models;
using FluentNHibernate.Mapping;
using System;



namespace QMS_API.Mappings
{

    public class MTypesMap : ClassMap<MTypes>
    {
        public MTypesMap()
        {
            Table("MTYPES");

            this.DynamicUpdate();

            Id(p => p.Id);

            Map(p => p.Description);
            Map(p => p.Status);
            Map(p => p.CreationId);
            Map(p => p.CreationTime);
            Map(p => p.LastModifiedId);
            Map(p => p.LastModifiedTime);
            Version(p => p.Version);
        }

    }
    public class MTypes_ReadMap : ClassMap<MTypes_Read>
    {
        public MTypes_ReadMap()
        {
            Table("MTYPES");

            this.DynamicUpdate();

            Id(p => p.Id);

            Map(p => p.Description);
            Map(p => p.Status);
            Map(p => p.CreationId);
            Map(p => p.CreationTime);
            Map(p => p.LastModifiedId);
            Map(p => p.LastModifiedTime);
            Version(p => p.Version);
        }

    }


}

