﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QMS_API.Helpers;

namespace QMS_API.Models
{
    public class QueueNumbers
    {
        DateTime _Time;
        public string QueueNo { get; set; }
        public string Type { get; set; }
        public string CurrentServingNo { get; set; }
        public virtual DateTime Time
        {
            get { return _Time; }
            set { _Time = value; }
        }
        public QueueNumbers()
        {

            DateTime now = DateTimeHelper.GetNow();
            Time = now;
        }
        public string DriverName { get; set; }
        public string PlateNo { get; set; }
    }

    public class QueueNoStatusMain
    {
        public string Counter { get; set; }
        public IList<QueueNoStatus> QueueNoStatus { get; set; }
    }

    public class QueueNoStatus
    {
        public string QueueType { get; set; }
        public int TotalQueue { get; set; }
        public string DriverName { get; set; }
        public string PlateNo { get; set; }
    }

    public class NextQueueNo
    {
        public string QueueNo { get; set; }
        public string QueueType { get; set; }
        public string DriverName { get; set; }
        public string PlateNo { get; set; }
    }

    public class QueueNoForQDS
    {
        public string queueNo { get; set; }
        public string counterNo { get; set; }
    }
}