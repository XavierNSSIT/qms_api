﻿using QMS_API.Models;
using FluentNHibernate.Mapping;
using System;



namespace QMS_API.Mappings
{

    public class MZonesMap : ClassMap<MZones>
    {
        public MZonesMap()
        {
            Table("MZONES");

            this.DynamicUpdate();

            Id(p => p.Id);

            Map(p => p.Description);
            Map(p => p.Status);
            Map(p => p.CreationId);
            Map(p => p.CreationTime);
            Map(p => p.LastModifiedId);
            Map(p => p.LastModifiedTime);
            Version(p => p.Version);
        }

    }



}

