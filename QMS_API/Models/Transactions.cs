﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QMS_API.Helpers;

namespace QMS_API.Models
{
    public class Transactions
    {
        DateTime _CreationTime;
        DateTime _LastModifiedTime;
        public virtual Guid Id { get; set; }
        public virtual int Number { get; set; }
        public virtual string MTypes_Id { get; set; }
        public virtual string MCounters_Id { get; set; }
        public virtual string ServedFlag { get; set; }
        public virtual DateTime LastModifiedTime
        {
            get { return _LastModifiedTime; }
            set { _LastModifiedTime = value; }
        }
        public virtual DateTime CreationTime
        {
            get { return _CreationTime; }
            set { _CreationTime = value; }
        }
        public Transactions()
        {
            DateTime now = DateTimeHelper.GetNow();
            _CreationTime = now;
            _LastModifiedTime = now;
        }
        public virtual int Version { get; set; }
        public virtual string Driver_Name { get; set;}
        public virtual string Driver_PlateNo { get; set; }
        public virtual string QueueNo { get; set; }
    }

    public class TransactionReport
    {
        public string QueueNo { get; set; }
        public string Type { get; set; }
        public string ServedFlag { get; set; }
        public string CounterServed { get; set; }
        public DateTime NoGeneratedTime { get; set; }
        public DateTime NoServedTime { get; set; }
        public string DriverName { get; set; }
        public string DriverPlateNo { get; set; }
    }
}