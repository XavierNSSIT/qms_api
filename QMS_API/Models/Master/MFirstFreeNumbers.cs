﻿using QMS_API.Helpers;
using System;

namespace QMS_API.Models
{
    public class MFirstFreeNumbers
    {
        DateTime _CreationTime;
        DateTime _LastModifiedTime;
        public virtual Guid Id { get; set; }
        public virtual string FirstFreeSettings_Id { get; set; }
        public virtual int RunningNo { get; set; }
        public virtual string LatestFlag { get; set; }
        public virtual string MTypes_Id { get; set; }
        public virtual string LastModifiedId { get; set; }
        public virtual string CreationId { get; set; }

        public virtual DateTime LastModifiedTime
        {
            get { return _LastModifiedTime; }
            set { _LastModifiedTime = value; }
        }
        public virtual DateTime CreationTime
        {
            get { return _CreationTime; }
            set { _CreationTime = value; }
        }

        public virtual long Version { get; set; }
        public MFirstFreeNumbers()
        {

            DateTime now = DateTimeHelper.GetNow();
            _CreationTime = now;
            _LastModifiedTime = now;
        }

    }

}
