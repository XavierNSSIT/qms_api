﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QMS_API.Models
{
    public class CounterPriority
    {
        DateTime _CreationTime;
        DateTime _LastModifiedTime;
        public virtual Guid Id { get; set; }
        public virtual string MCounters_Id { get; set; }
        public virtual string MTypes_Id { get; set; }
        public virtual string Priority { get; set; }
        public virtual string Status { get; set; }
        public virtual string CreationId { get; set; }
        public virtual string LastModifiedId { get; set; }
        public virtual DateTime LastModifiedTime
        {
            get { return _LastModifiedTime; }
            set { _LastModifiedTime = value; }
        }
        public virtual DateTime CreationTime
        {
            get { return _CreationTime; }
            set { _CreationTime = value; }
        }
        public virtual int Version { get; set; }
    }

    public class CounterPriority_Read
    {
         public virtual Guid Id { get; set; }
        public virtual string MCounters_Id { get; set; }
        public virtual string MTypes_Id { get; set; }
        public virtual string Priority { get; set; }
        public virtual string CreationId { get; set; }
        public virtual string LastModifiedId { get; set; }
        public virtual string Status { get; set; }

        public virtual DateTime LastModifiedTime{ get; set; }
        public virtual DateTime CreationTime{ get; set; }
        public virtual int Version { get; set; }

        public virtual MCounters_Read Counter { get; set; }

        public virtual MTypes_Read Type { get; set; }
    }

    
}