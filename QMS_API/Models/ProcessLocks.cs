﻿using System;

namespace QMS_API.Models
{
    public class ProcessLocks
    {
        public const int PaymentBatch = 1;
        public const int CompanyBatch = 2;
        public const int PaymentDetailBatch = 3;
     
        public virtual int Id { get; set; }
        public virtual string ProcessName { get; set; }
    }
}