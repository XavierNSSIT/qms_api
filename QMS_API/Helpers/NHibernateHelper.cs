using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using log4net;
using QMS_API.Models;
using System.Diagnostics;
using System.Configuration;
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace QMS_API.Helpers
{
    public class NHibernateHelper
    {
        private static ISessionFactory _sessionFactory;
        private static string strDBType = System.Configuration.ConfigurationManager.AppSettings["MyDatabase"];
        private static string strConn = ConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;


        private static ISessionFactory SessionFactory
        {
            get
            {

                if (_sessionFactory == null)
                {

                    strDBType=strDBType.ToLower();

                    if (strDBType == "mssql2005")
                    {
                        _sessionFactory= Fluently.Configure()
                          .Database(MsSqlConfiguration.MsSql2008.ShowSql().FormatSql().ConnectionString(strConn))
                          .Mappings(m => m.FluentMappings.AddFromAssemblyOf<MCounters>())
                          .BuildSessionFactory();                         
                    }
                    else {
                     
                        _sessionFactory = Fluently.Configure()
                        .Database(OracleClientConfiguration.Oracle10
                        .ConnectionString(strConn)
                        .Driver<NHibernate.Driver.OracleClientDriver>().ShowSql().FormatSql())
                        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<MCounters>())
                        .BuildSessionFactory();
                    }             
                }

                return _sessionFactory;
            }
        }

        private static void InitializeSessionFactory()
        {
         
        }

        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }
    }
}