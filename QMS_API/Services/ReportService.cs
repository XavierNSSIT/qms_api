﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QMS_API.Models;
using NHibernate;
using FluentNHibernate;
using QMS_API.Helpers;
using NHibernate.Transform;

namespace QMS_API.Services
{
    public class ReportService
    {
        public IList<TransactionReport> TxnReportGet(DateTime dateFrom, DateTime dateTo)
        {
            IList<TransactionReport> reportData = new List<TransactionReport>();
            TransactionReport report = null;

            using (ISession session = NHibernateHelper.OpenSession())
            {
                reportData = session.QueryOver<Transactions>()
                            .SelectList(list => list
                                .Select(x => x.QueueNo).WithAlias(() => report.QueueNo)
                                .Select(x => x.MTypes_Id).WithAlias(() => report.Type)
                                .Select(x => x.MCounters_Id).WithAlias(() => report.CounterServed)
                                .Select(x => x.ServedFlag).WithAlias(() => report.ServedFlag)
                                .Select(x => x.CreationTime).WithAlias(() => report.NoGeneratedTime)
                                .Select(x => x.LastModifiedTime).WithAlias(() => report.NoServedTime)
                                .Select(x => x.Driver_Name).WithAlias(() => report.DriverName)
                                .Select(x => x.Driver_PlateNo).WithAlias(() => report.DriverPlateNo))
                            .Where(x => x.CreationTime >= dateFrom)
                            .And(x => x.CreationTime < dateTo)
                            .TransformUsing(Transformers.AliasToBean<TransactionReport>())
                            .List<TransactionReport>();
            }

            return reportData;
        }


    }
}