﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace QMS_API.Helpers
{
    public static class DateTimeHelper
    {            

        public static DateTime GetNow()
        {
            return DateTime.Now;
        }

        
        public static string ToDateOnlyFormat(this DateTime timeForFormat)
        {
            return timeForFormat.ToString("dd MMM yyyy");            
        }

        
        public static string ToHTMLFormat(this DateTime timeForFormat)
        {
           return timeForFormat.ToString("dd MMM yyyy    hh:mm:ss tt").Replace(" ", "&nbsp;");
        }

        public static string ToJsonFormat(this DateTime timeForFormat)
        {
            return "LocalTime=" + timeForFormat.ToString("dd MMM yyyy hh:mm:ss tt");
        }

        public static string ToDateTimeFormat(this DateTime timeForFormat)
        {

            return timeForFormat.ToString("dd MMM yyyy hh:mm:ss tt");
        }

        public static string ToTimeFormat(this DateTime timeForFormat)
        {

            return timeForFormat.ToString("HH:mm:ss");
        }


        public static string ToSQLFormat(this DateTime timeForConvert)
        {            
            return timeForConvert.ToString("yyyy-MM-dd HH:mm:ss");
        }
             
        public static DateTime GetMinDate() {
            return System.Data.SqlTypes.SqlDateTime.MinValue.Value;
        }


        public static string ToReportNameLocalFormat(this DateTime timeForConvert)
        {
            return DateTime.Now.ToString("yyyyMMdd_HHmmss");

        }


        public static bool IsDate(string dateValue, string dateFormat,out DateTime dateResult)
        {
            bool result = false;
            dateResult = GetMinDate();
            if (DateTime.TryParseExact(dateValue, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateResult))
            {
                result = true;
            }

            return result;
        }

    
    }
       
}