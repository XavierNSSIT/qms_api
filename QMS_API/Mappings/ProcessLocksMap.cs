﻿using QMS_API.Models;
using FluentNHibernate.Mapping;
using System;


namespace QMS_API.Mappings
{
    public class ProcessLocksMap : ClassMap<ProcessLocks>
    {
        public ProcessLocksMap()
        {
            Table("PROCESSLOCKS");

            this.DynamicUpdate();
            this.DynamicInsert();

            Id(p => p.Id);
            Map(p => p.ProcessName);

        }

    }
}