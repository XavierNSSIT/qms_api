﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QMS_API.Helpers;

namespace QMS_API.Models
{
    public class CurrentServingNumbers
    {
        DateTime _CreationTime;
        public virtual Guid Id { get; set; }
        public virtual string MTypes_Id { get; set; }
        public virtual int Number { get; set; }
        public virtual DateTime CreationTime
        {
            get { return _CreationTime; }
            set { _CreationTime = value; }
        }
        public virtual int Version { get; set; }
    }
}